list = ['mix', 'xyz', 'apple', 'xanadu', 'aardvark']

l1 = sorted([ ele for ele in list if ele[0] == 'x'])
l2 = sorted([ ele for ele in list if ele[0] != 'x'])
#l1.sort()
#l2.sort()
l1.extend(l2)
print("List: ", list)
print("Sorted list: ", l1)
