#list1 = [2, 2, 3, 3, 3]
list1 = [3,3,3,2,2,1,2,2,3,3,3,5,5,5,5,1]

print("List: ", list1)

for index,ele in enumerate(list1):
    if index>=len(list1)-1:
        continue;
    it = iter( (list1[ (index+1): ]) )

    # try:
    while( (next(it)) == ele  ):
            list1.pop(index+1)
    # except Exception: # to catch traceback
        # break;
    #end of try catch
#end of for
print("List after adjacent duplicates removed: ", list1)
