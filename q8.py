string = "Python is a widely used high-level programming language for general-purpose programming, created by Guido van Rossum and first released in 1991. An interpreted language, Python has a design philosophy which emphasizes code readability (notably using whitespace indentation to delimit code blocks rather than curly braces or keywords), and a syntax which allows programmers to express concepts in fewer lines of code than possible in languages such as C++ or Java. The language provides constructs intended to enable writing clear programs on both a small and large scale .Python features a dynamic type system and automatic memory management and supports multiple programming paradigms, including object-oriented, imperative, functional programming, and procedural styles. It has a large and comprehensive standard library. Python interpreters are available for many operating systems, allowing Python code to run on a wide variety of systems. CPython, the reference implementation of Python, is open source software and has a community-based development model, as do nearly all of its variant implementations. CPython is managed by the non-profit Python Software Foundation."

def clean_punctuation(str):
    clean_word = ""
    for char in str:
        if( not(char.isalnum() or char=='+' ) ):
            continue;
        clean_word += char

    return clean_word.lower()
    pass
#End of clear punctuation fn

def add_to_dictionary(keyword,val,dict):                    #function takes word as keyword and the following word as value and adds it to the dictionary

    if keyword in dict.keys():
        try:                                            #index function throws ValueError when item not found in a list
            dict[keyword].index(val)
        except ValueError:
            dict[keyword].append(val)
        else:
            pass
    else:
        dict[keyword] = [val]

    return dict
    pass
#End of add to dictionary fn

dictionary = {}
list_of_words = string.split()

for index_outer,word in enumerate(list_of_words):                      # ind is loop counter for list_of_words

    if (word[-1]=='.' or word[-1]==',' ):   #condition 1: if . or ,  appears, it means end of sentence so no likely words to follow if brackets reqd add here
        continue

    elif word.find("-")!=-1:                                  #condition 2 : checking for hyphenated words
        list_of_individual_words = word.split("-")
        last_hyphen_word = ""

        #For loop to individually add hyphenated words to dictionary except the last part
        for index,sub in enumerate(list_of_individual_words):   #index is loop counter for list_of_individual_words
            if index>=len(list_of_individual_words)-1:
                last_hyphen_word = sub
                continue;
            dictionary = add_to_dictionary(clean_punctuation(sub), clean_punctuation(list_of_individual_words[index+1]), dictionary)
        #End of for loop

        dictionary = add_to_dictionary(clean_punctuation(last_hyphen_word), clean_punctuation(list_of_words[index_outer+1]), dictionary)  #to add last part of hyphenated word to add_to_dictionary

    else:
        dictionary = add_to_dictionary(clean_punctuation(word), clean_punctuation(list_of_words[index_outer+1]), dictionary)
#End of for loop

print("String: \n", string)
print("\n Dictionary: ")
print(dictionary)
