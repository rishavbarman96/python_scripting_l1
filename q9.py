import re
string = '''
FastEthernet0/0	192.168.1.242	YES 	manual up	up
FastEthernet1/0        unassigned	YES 	unset		down
Serial2/0              	192.168.1.250	YES 	manual up	up
Serial3/0              	192.168.1.233	YES 	manual up	up
FastEthernet4/0        unassigned	YES 	unset  		down
FastEthernet5/0        unassigned	YES        unset 		down
'''
print("Inputted string from router:\n", string)
pattern = re.compile(r'([A-Za-z]+\d/\d)\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|unassigned)\s+(YES|NO)\s+(manual up|unset)\s+(up|down)')
sub_pattern = pattern.sub(r'\1   \4', string)

print("Required values:")
print(sub_pattern)
