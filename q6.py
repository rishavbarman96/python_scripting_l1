bookstore={"New Arrivals":{"COOKING":["Everyday Italian","Giada De Laurentiis","2005","30.00"],"CHILDREN":["Harry Potter”, J K. Rowling","2005","29.99"],"WEB":["Learning XML","Erik T. Ray","2003","39.95"]}}

print("Dictionary: \n",bookstore)
booklist = []

for bay,val in bookstore.items():
    for genre,book in val.items():
        booklist.append(book)
#end of for

print("\nThe required booklist is: \n",booklist)
