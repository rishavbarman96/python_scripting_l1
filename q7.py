
string = "Python is a widely used high-level programming language for general-purpose programming, created by Guido van Rossum and first released in 1991. An interpreted language, Python has a design philosophy which emphasizes code readability (notably using whitespace indentation to delimit code blocks rather than curly braces or keywords), and a syntax which allows programmers to express concepts in fewer lines of code than possible in languages such as C++ or Java. The language provides constructs intended to enable writing clear programs on both a small and large scale .Python features a dynamic type system and automatic memory management and supports multiple programming paradigms, including object-oriented, imperative, functional programming, and procedural styles. It has a large and comprehensive standard library. Python interpreters are available for many operating systems, allowing Python code to run on a wide variety of systems. CPython, the reference implementation of Python, is open source software and has a community-based development model, as do nearly all of its variant implementations. CPython is managed by the non-profit Python Software Foundation."

def add_to_dictionary(key,dict):

    if key in dict.keys():
        dict[key]+=1
    else:
        dict[key] = 1

    return dict
    pass
#End of add_to_dictionary fn


def clean_punctuation(str):
    clean_word = ""
    for char in str:
        if( not(char.isalnum() or char=='+' ) ):
            continue;
        clean_word += char

    return clean_word.lower()
    pass
#End of clear punctuation fn


dictionary = {}
for word in string.split():
    if word.find("-")!=-1: #checking for hyphenated words
        for sub in word.split("-"):
            sub = clean_punctuation(sub)
            dictionary = add_to_dictionary(sub,dictionary)

    else:
        word = clean_punctuation(word)
        dictionary = add_to_dictionary(word,dictionary)
print("String: \n", string)
print("\n Dictionary: \n")
print(dictionary)
