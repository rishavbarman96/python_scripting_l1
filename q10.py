final_posn = 1000000000                                                        #final position
fin = abs(final_posn)

sum = 0
i = 0
                                                                      #i determines Step no
while (sum < fin or (sum - fin) %2 != 0) :
    i+=1
    sum+=i

print("Min no of steps req to reach ", final_posn," is:")
print(i)
